from base.models import Client, Mailing, Message
from django.utils import timezone
import requests
CLIENT = 'http://127.0.0.1:8080'


def watch():
    print('SENT '* 5)
    clients = Client.objects.all()
    mailings = Mailing.objects.all()

    now = timezone.now()
    for mailing in mailings:
        filters = []
        if mailing.date_start < now < mailing.date_finish:
            tags = [m.strip() for m in mailing.filters.split(',')]
            filters.extend(tags)
        for client in clients:
            if client.tag in filters or client.operator in filters:
                sent_to_client(client, mailing)

def sent_to_client(client, mailing):
    existing_message = Message.objects.filter(
        id_mailing=mailing.id, id_client=client.id, status=201)
    if existing_message:
        return
    try:
        response = requests.post(CLIENT)
    except:
        print('Client server not work')
        return
    msg = Message(
        date_sent=timezone.now(),
        status=response.status_code,
        id_mailing=mailing,
        id_client=client
    )
    msg.save()
    if response.status_code == 201:
        print('sent -> ', client.phone, 'text:', mailing.text)  # sent
    else:
        print('bad sent => ', client.phone)
