from django.shortcuts import render


def index(request):
    data = {
        'urls': [
            'get_clients',
            'add_client',
            'update_client',
            'get_mailings',
            'add_mailing',
            'get_statistic_mailings',
            'get_messages',
        ]
    }
    return render(request, 'index.html', data)
