"""
WSGI config for server project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'server.settings')

application = get_wsgi_application()


import threading
from time import sleep
from . import watcher

def background():
    while True:
        watcher.watch()
        sleep(60)

b = threading.Thread(name='background', target=background)
b.start()