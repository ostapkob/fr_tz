from django.db import models
from datetime import datetime
import uuid

# https://docs.djangoproject.com/en/4.1/topics/db/models/


class Client(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    phone = models.CharField(
        max_length=11, unique=True, null=False)
    operator = models.CharField(max_length=100, null=False)
    tag = models.CharField(max_length=100, null=False)
    gmt = models.IntegerField(null=False)

    def __str__(self):
        return f'{self.phone, self.operator, self.tag, self.gmt }'


class Mailing(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    date_start = models.DateTimeField(null=False)
    text = models.CharField(max_length=1000, null=False)
    filters = models.CharField(max_length=100, null=False)  # spit comma
    date_finish = models.DateTimeField(null=False)
    def __str__(self):
        return f'{self.date_start, self.text, self.filters, self.date_finish }'


class Message(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    date_sent = models.DateTimeField(null=False, default=datetime.now)  # TODO utcnow
    status = models.IntegerField(null=False, default=None)
    id_mailing = models.ForeignKey(Mailing, default=None, on_delete=models.CASCADE)
    id_client = models.ForeignKey(
        Client, default=None, on_delete=models.CASCADE)
