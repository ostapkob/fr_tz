from django.urls import path
from . import views


urlpatterns = [
    path('get_clients', views.getClient),
    path('add_client/', views.addClient),
    path('update_client/', views.updateClient),
    path('delete_client/<phone>', views.deleteClient),
    path('get_mailings', views.getMailing),
    path('add_mailing/', views.addMailing),
    path('get_statistic_mailings', views.getStatisticMailing),
    path('get_statistic_mailings/<id>', views.getDetalStatisticMailing),
    path('get_messages', views.getMessages),
]
