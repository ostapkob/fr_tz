from rest_framework import serializers
from base.models import Client, Mailing, Message

# https://www.geeksforgeeks.org/modelserializer-in-serializers-django-rest-framework/


class ClientSerializer(serializers.ModelSerializer):  # TODO
    class Meta:
        model = Client
        fields = '__all__'
        # fields = ["phone", "operator", "tag", "gmt"]


class MailingSerializer(serializers.ModelSerializer):  # TODO
    class Meta:
        model = Mailing
        fields = '__all__'

class MessageSerializer(serializers.ModelSerializer):  # TODO
    class Meta:
        model = Message
        fields = '__all__'


class ClientUpdateSerializer(serializers.ModelSerializer): 
    class Meta:
        model = Client
        fields = ['operator', 'tag', 'gmt' ]
        validators = []