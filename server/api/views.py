from pydoc import cli
from urllib import response
from rest_framework.response import Response
from rest_framework.decorators import api_view
from base.models import Client, Mailing, Message
from .serializes import ClientSerializer, ClientUpdateSerializer, MailingSerializer, MessageSerializer
from rest_framework import status


@api_view(['GET'])
def getClient(request):
    items = Client.objects.all()
    serializer = ClientSerializer(items, many=True)
    return Response(serializer.data)


@api_view(['POST'])
def addClient(request):
    serializer = ClientSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response(serializer.errors)


@api_view(['GET'])
def getMailing(request):
    items = Mailing.objects.all()
    serializer = MailingSerializer(items, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def getStatisticMailing(request):
    mailings = Mailing.objects.all()
    mailings_ids = [i.id for i in mailings]
    messages = Message.objects.all()
    statuses = set(m.status for m in messages)
    data = {str(mailings_id): {str(status): 0 for status in statuses}
            for mailings_id in mailings_ids}
    for message in messages:
        data[str(message.id_mailing.id)][str(message.status)] += 1
    return Response(data)


@api_view(['GET'])
def getDetalStatisticMailing(request, id):
    try:
        messages = Message.objects.filter(id_mailing=id)
        data = {str(message.id_client.phone): [] for message in messages}

        for message in messages:
            data[str(message.id_client.phone)].append(
                {'date_sent': message.date_sent, 'status': message.status})

        return Response(data)
    except:
        return Response("not found", status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
def addMailing(request):
    serializer = MailingSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response(serializer.errors)


@api_view(['GET'])
def getMessages(request):
    items = Message.objects.all()
    serializer = MessageSerializer(items, many=True)
    return Response(serializer.data)


@api_view(['PUT'])
def updateClient(request):
    try:
        client = Client.objects.get(phone=request.data['phone'])
    except:
        return Response("not found", status=status.HTTP_404_NOT_FOUND)
    serializer = ClientUpdateSerializer(data=request.data)
    if serializer.is_valid():
        client.phone = request.data['phone']
        client.operator = request.data['operator']
        client.tag = request.data['tag']
        client.gmt = request.data['gmt']
        client.save()
        return Response(serializer.data)
    return Response(serializer.errors)


@api_view(['DELETE'])
def deleteClient(request, phone):
    try:
        client = Client.objects.get(phone=phone)
        client.delete()
        return Response(f'{phone} was delete')
    except:
        return Response("not found", status=status.HTTP_404_NOT_FOUND)
