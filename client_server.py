#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Python 3 server example
from http.server import BaseHTTPRequestHandler, HTTPServer
import time
import random


hostName = "localhost"
serverPort = 8080

class MyServer(BaseHTTPRequestHandler):
    def do_POST(self):
        rnd = random.randint(0, 2)
        if rnd==0:
            self.send_response(201) #create
        else:
            self.send_response(408) #timeout
        self.send_header("Content-type", "text/html")
        self.end_headers()

if __name__ == "__main__":        
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
